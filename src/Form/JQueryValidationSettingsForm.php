<?php

namespace Drupal\jquery_form_validation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implement Class to jQuery validation setting form.
 */
class JQueryValidationSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jquery_form_validation_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['jquery_form_validation.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('jquery_form_validation.settings');

    $form['heading'] = [
      '#type' => 'markup',
      '#markup' => 'Read the document and apply the validations as per <a href="https://jqueryvalidation.org/documentation/">jQuery Validation Library Doc</a>. Here is the <a href="test-validation-example-form" target="_blank">Demo form </a>',
    ];
    $def_value = '{
  "test-settings-form": {
    "rules": {
      "name": "required",
      "email": {
        "required": true,
        "email": true
      },
      "message": "required",
      "dropdown": "required",
      "check_box": "required",
      "choice": "required",
      "body": "required",
      "current_date_time[date]": "required",
      "numeric_field": {
        "required": true,
        "step" : 10
      },
      "current_date_time[time]": "required"
    },
    "messages": {
      "name": "Please enter your name",
      "email": "Please enter a valid email address",
      "message": "Please enter a your message",
      "dropdown": "Please select a value from dropdown",
      "check_box": "Please select the checkbox field",
      "choice": "Please select the gender",
      "body": "Please enter the body",
      "current_date_time[date]": "Please select date",
      "numeric_field": {
        "required": "Enter number"
      },
      "current_date_time[time]": "Please enter a time"
    },
    "submitHandler": {
      "body": {
        "message": "Please enter body text"
      }
    }
  }
}';

    $form['demo_validation_enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Demo form'),
      '#description' => $this->t('The demo JSON validation will work or not.'),
      '#default_value' => !empty($config->get('demo_validation_enable')) ? $config->get('demo_validation_enable') : 1,
    ];

    $form['demo_validation_data'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Validation json'),
      '#rows' => 15,
      '#description' => $this->t('Validation json for the demo form'),
      '#default_value' => !empty($config->get('demo_validation_data')) ? $config->get('demo_validation_data') : $def_value,
    ];

    $form['json_apply'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('JSON validation'),
      '#description' => $this->t('The JSON validation will work or not.'),
      '#default_value' => $config->get('json_apply'),
    ];

    if ($config->get('names_fieldset') == NULL) {
      $default_params = [];
    }
    else {
      $default_params = $config->get('names_fieldset');
      unset($default_params['actions']);
    }
    $num_names = $form_state->get('num_names') ? $form_state->get('num_names') : count($default_params);
    // Gather the number of names in the form already.
    // We have to ensure that there is at least one name field.
    if ($num_names === NULL) {
      $name_field = $form_state->set('num_names', 1);
      $num_names = 1;
    }
    else {
      $name_field = $form_state->set('num_names', $num_names);
    }
    $form['#tree'] = TRUE;
    $form['names_fieldset'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Validation values'),
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    for ($inc = 0; $inc < $num_names; $inc++) {
      $form['names_fieldset'][$inc]['score'] = [
        '#type' => 'item',
        '#markup' => t('Form nu - @inc', ['@inc' => $inc]),

      ];
      $form['names_fieldset'][$inc]['validation_json'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Validation json'),
        '#rows' => 15,
        '#description' => $this->t('You can add the validation content with the json format'),
        '#placeholder' => t('{
  "form-id-@inc": {
    "rules": {
      "name": "required",
      "email": {
        "required": "true",
        "email": "true"
      },
      "password": {
        "required": "true",
        "minlength": 5
      }
    },
    "messages": {
      "name": "Please enter your name",
      "email": "Please enter a valid email address",
      "password": {
        "required": "Please provide a password",
        "minlength": "Your password must be at least 5 characters long"
      }
    },
    "submitHandler": {
      "body": "Please enter the body"
    }
  }
}', ['@inc' => $inc]),
        '#default_value' => isset($default_params[$inc]) ? $default_params[$inc]['validation_json'] : "",
      ];
    }
    $form['names_fieldsets']['actions'] = [
      '#type' => 'actions',
    ];
    $form['names_fieldsets']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'names-fieldset-wrapper',
      ],
    ];
    // If there is more than one name, add the remove button.
    if ($num_names > 1) {
      $form['names_fieldset']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove one'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'names-fieldset-wrapper',
        ],
      ];
    }

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['names_fieldset'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    $add_button = $name_field + 1;
    $form_state->set('num_names', $add_button);
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove one" button.
   *
   * Decrements the max counter and causes a form rebuild.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    if ($name_field > 1) {
      $remove_button = $name_field - 1;
      $form_state->set('num_names', $remove_button);
    }
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('jquery_form_validation.settings');

    $config->set('json_apply', $form_state->getValue('json_apply'));
    $config->set('names_fieldset', $form_state->getValue('names_fieldset'));
    $config->set('demo_validation_enable', $form_state->getValue('demo_validation_enable'));
    $config->set('demo_validation_data', $form_state->getValue('demo_validation_data'));

    $config->save();

    \Drupal::messenger()->addMessage(t('Custom validations has been saved.'));
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

}
