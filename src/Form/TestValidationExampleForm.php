<?php

namespace Drupal\jquery_form_validation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implement Class to test validation example form.
 */
class TestValidationExampleForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'test_validation_example_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['jquery_form_validation.validation_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['name'] = [
      '#title' => $this->t('Name'),
      '#type' => 'textfield',
      '#description' => $this->t('This is validation for name field.'),
    ];

    $form['items'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Items'),
      '#prefix' => '<div id="items-wrapper">',
      '#suffix' => '</div>',
      '#tree' => TRUE,
    ];

    $num_items = $form_state->get('num_items') ?: 1;

    for ($i = 0; $i < $num_items; $i++) {
      $form['items'][$i]['text_item'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Text @num', ['@num' => $i + 1]),
      ];
      $form['items'][$i]['email_item'] = [
        '#type' => 'email',
        '#title' => $this->t('Email item @num', ['@num' => $i + 1]),
      ];
    }

    $form['add_item'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another item'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addMoreCallback',
        'wrapper' => 'items-wrapper',
      ],
    ];

    if ($num_items > 1) {
      $form['remove_item'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove item'),
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addMoreCallback',
          'wrapper' => 'items-wrapper',
        ],
      ];
    }

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#description' => $this->t('This is validation for email field.'),
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#description' => $this->t('This is validation for message field.'),
    ];

    $form['dropdown'] = [
      '#type' => 'select',
      '#title' => $this->t('Drop down'),
      '#options' => [
        '' => 'Select',
        'A' => 'A',
        'B' => 'B',
        'C' => 'C',
      ],
      '#description' => $this->t('This is validation for dropdown field.'),
    ];

    $form['check_box'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Checkbox'),
      '#description' => $this->t('This is validation for checkbox field.'),
    ];

    $form['choice'] = [
      '#type' => 'radios',
      '#title' => $this->t('Gender'),
      '#description' => $this->t('Specify gender.'),
      '#options' => [
        $this->t('Male'),
        $this->t('Female'),
      ],
    ];

    $form['numeric_field'] = [
      '#type' => 'textfield',
      '#attributes' => [
        ' type' => 'number',
      ],
      '#title' => 'Numeric field',
    ];

    $form['current_date_time'] = [
      '#title' => 'Current date and time',
      '#type' => 'datetime',
      '#date_date_element' => 'date',
      '#date_time_element' => 'time',
    ];

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#format' => 'full_html',
      '#description' => $this->t('For body.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $num_items = $form_state->get('num_items') ?: 1;
    $form_state->set('num_items', $num_items + 1);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $num_items = $form_state->get('num_items') ?: 1;
    if ($num_items > 1) {
      $form_state->set('num_items', $num_items - 1);
    }
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function addMoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['items'];
  }

}
