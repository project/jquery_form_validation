# JQuery form validation

The JQuery form validation module improve the custom form validation that helps
to apply the validation for user / developer to use it. The validation form uses
the json data for multiple forms use the different area for apply the validation.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/jquery_form_validation).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search/jquery_form_validation).

## Table of contents

- Installation
- Configuration

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

Configure the jquery form validation at (/admin/config/form-validation).