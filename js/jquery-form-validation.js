/**
 * @file
 * Attaches behaviors for the Clientside Validation jQuery module.
 */

(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.validationBehavior = {
    attach: function (context, settings) {
      if (drupalSettings.validationData !== undefined) {
        let validationData = drupalSettings.validationData;
        let formId = validationData.formKey;
        let extractFormId = formId.split("--");
        let newFormId = extractFormId[0];
        let rules = validationData.rulesAndMessages[newFormId].rules;
        let messages = validationData.rulesAndMessages[newFormId].messages;
        let submitHandler = validationData.rulesAndMessages[newFormId].submitHandler;
        let errorClass = validationData.rulesAndMessages[newFormId].errorClass;
        let onfocusout = validationData.rulesAndMessages[newFormId].onfocusout;
        let onkeyup = validationData.rulesAndMessages[newFormId].onkeyup;
        let onclick = validationData.rulesAndMessages[newFormId].onclick;
        let focusInvalid = validationData.rulesAndMessages[newFormId].focusInvalid;
        let focusCleanup = validationData.rulesAndMessages[newFormId].focusCleanup;
        let errorElement = validationData.rulesAndMessages[newFormId].errorElement;
        let wrapper = validationData.rulesAndMessages[newFormId].wrapper;
        $("#" + formId).validate({
          rules,
          messages,
          submitHandler: function (form) {
            var flag = flagSubmit = true;
            if (submitHandler !== undefined) {
              if(typeof CKEDITOR != 'undefined') {      // CKEDITOR 4
                const instanceObj = Object.keys(CKEDITOR.instances);
                for(var keys in instanceObj) {
                  for (var i in submitHandler) {
                    let editor = 'edit-' + i + '-value';
                    let underscoredName = i.replace("-", "_");
                    let hyphenName = i.replace("_", "-");
                    let inst = instanceObj[keys];
                    let extractedFieldName = inst.split("--");
                    let fieldName = extractedFieldName[0];
                    let editorHyphenedName = 'edit-' + hyphenName + '-value';
                    if(fieldName == editorHyphenedName) {
                      var editorData = CKEDITOR.instances[inst].getData();
                      if (!editorData.trim()) {
                        flagSubmit = true;
                        if(submitHandler[i].message != 'undefined') {
                          $('textarea[name="' + underscoredName + '\\[value\\]"]').after('<label id="' + editor + '-error" class="error" for="' + editor + '" style="">' + submitHandler[i].message + '</label>');
                          if (flag) {
                            if($('#drupal-modal').length > 0) {
                              $('#drupal-modal').scrollTop($("#cke_" + inst).offset().top);
                            } else {
                              $('html,body').scrollTop($("#cke_" + inst).offset().top - 300);
                            }
                            flag = false;
                            flagSubmit = true;
                          }
                        }
                        break;
                      } else {
                        flagSubmit = false;
                        $("#" + editor + "-error").remove();
                      }
                    }
                  }
                }
              }
              else if(typeof CKEditor5 != 'undefined') {        // CKEDITOR 5
                for (var i in submitHandler) {
                  let editor = 'edit-' + i + '-value';
                  let underscoredName = i.replace("-", "_");
                  let hyphenName = i.replace("_", "-");
                  let editorHyphenedName = 'edit-' + hyphenName + '-value';
                  console.log('underscoredName', underscoredName);
                  let inst = $('textarea[name="' + underscoredName + '\\[value\\]"]').attr('id');
                  let extractedFieldName = inst.split("--");
                  let fieldName = extractedFieldName[0];
                  console.log('inst', inst);
                  console.log('fieldName', fieldName);
                  console.log('editorHyphenedName', editorHyphenedName);
                  if(fieldName == editorHyphenedName) {
                    var editorData = document.querySelector('#' + inst).innerText;
                    if (!editorData.trim()) {
                      flagSubmit = true;
                      if(submitHandler[i].message != 'undefined') {
                        if($('#' + editor + '-error').length == 0) {
                          $('textarea[name="' + underscoredName + '\\[value\\]"]').after('<label id="' + editor + '-error" class="error" for="' + editor + '" style="">' + submitHandler[i].message + '</label>');
                        } else {
                          $("#" + editor + "-error").remove();
                          $('textarea[name="' + underscoredName + '\\[value\\]"]').after('<label id="' + editor + '-error" class="error" for="' + editor + '" style="">' + submitHandler[i].message + '</label>');
                        }
                        if (flag) {
                          if($('#drupal-modal').length > 0) {
                            $('#drupal-modal').scrollTop($('textarea[name="' + underscoredName + '\\[value\\]"]').parent().offset().top);
                            $('textarea[name="' + underscoredName + '\\[value\\]"]').addClass('error');
                          } else {
                            $('html,body').scrollTop($('textarea[name="' + underscoredName + '\\[value\\]"]').parent().offset().top);
                            $('textarea[name="' + underscoredName + '\\[value\\]"]').addClass('error');
                          }
                          flag = false;
                          flagSubmit = true;
                        }
                      }
                      // break;
                    } else {
                      flagSubmit = false;
                      $("#" + editor + "-error").remove();
                    }
                  }
                }
              }
            }
            else {
              flagSubmit = false;
            }
            if(flagSubmit == false) {
              form.submit();
            }
          },
          errorClass,
          onfocusout,
          onkeyup,
          onclick,
          focusInvalid,
          focusCleanup,
          errorElement,
          wrapper,
        });

        for(let child in rules) {
          if(rules[child].fieldset) {
            const ruleObj = Object.keys(rules[child]);
            for (let ruleKey in ruleObj) {
              if(ruleObj[ruleKey] != 'fieldset') {
                let childSelector = ruleObj[ruleKey].replace("_", "-");
                let childrens = $('fieldset[data-drupal-selector="edit-' + child + '"] input[data-drupal-selector^="edit-' + child + '-"]input[data-drupal-selector$="-' + childSelector + '"]');
                childrens.each(function (ind, el) {
                  let thisId = $(this).attr('id');
                  let extractedFieldName = thisId.split("--");
                  if(rules[child].firstRequired) {
                    if(ind == 0) {
                      stringRule(rules, child, ruleObj, ruleKey, messages, thisId);
                      objectStringRule(rules, child, ruleObj, ruleKey, messages, thisId);
                      objectRule(rules, child, ruleObj, ruleKey, messages, thisId);
                    }
                    else {
                      objectRule(rules, child, ruleObj, ruleKey, messages, thisId, required = false);
                    }
                  } else {
                    stringRule(rules, child, ruleObj, ruleKey, messages, thisId);
                    objectStringRule(rules, child, ruleObj, ruleKey, messages, thisId);
                    objectRule(rules, child, ruleObj, ruleKey, messages, thisId);
                  }
                });
              }
            }
          }
        }

        function stringRule(rules, child, ruleObj, ruleKey, messages, thisId) {
          if(typeof rules[child][ruleObj[ruleKey]] == 'string' && typeof messages[child][ruleObj[ruleKey]] == 'string') {
            $("#" + thisId).rules( "add", {
              required: true,
              messages: {
                required: messages[child][ruleObj[ruleKey]]
              }
            });
          }
          return;
        }

        function objectStringRule(rules, child, ruleObj, ruleKey, messages, thisId) {
          if(typeof rules[child][ruleObj[ruleKey]] == 'object' && typeof messages[child][ruleObj[ruleKey]] == 'string') {
            let messagesObj = {};
            let validationObj = rules[child][ruleObj[ruleKey]];
            let validationMsg = messages[child][ruleObj[ruleKey]];
            let validationData = validationObj;
            if(validationMsg != 'undefined') {
              messagesObj['required'] = validationMsg;
              validationData['messages'] = messagesObj;
              $("#" + thisId).rules("add", validationData);
            }
          }
          return;
        }
        function objectRule(rules, child, ruleObj, ruleKey, messages, thisId, required) {
          if(typeof rules[child][ruleObj[ruleKey]] == 'object' && typeof messages[child][ruleObj[ruleKey]] == 'object') {
            let validationObj = rules[child][ruleObj[ruleKey]];
            if(required == false) {
              delete validationObj.required;
            }
            let validationMsg = messages[child][ruleObj[ruleKey]];
            let validationData = validationObj;
            validationData['messages'] = validationMsg;
            $("#" + thisId).rules("add", validationData);
          }
          return;
        }
      }
    }
  }
})(jQuery, Drupal, drupalSettings);
