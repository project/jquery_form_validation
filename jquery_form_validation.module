<?php

/**
 * @file
 * Hook implementations for the Form validation module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;

/**
 * Implements hook_help().
 */
function jquery_form_validation_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.jquery_form_validation':
      $variables = [
        ':library_url' => 'https://jqueryvalidation.org/documentation/',
        ':test_form' => Url::fromRoute('jquery_form_validation.validation_settings', [])->toString(),
        ':setting_form' => Url::fromRoute('jquery_form_validation.settings_form', [])->toString(),
      ];
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('The JQuery form validation module enhances the form validation by entering the data into the json format to validate the form using the <a href=":library_url" title="jQuery validation library">jQuery validation library</a> . It helps to apply the validaion into the custom forms . You can access the <a href=":test_form" title="Test form">test form</a> and apply the validation into the <a href=":setting_form" title="Setting form">setting form</a>', $variables) . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<p>' . t('The jQuery form validation greatly improves the user / developer experience for those who regularly interact with it.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_form_alter().
 */
function jquery_form_validation_form_alter(array &$form, FormStateInterface $form_state) {
  $route_name = \Drupal::routeMatch()->getRouteName();
  if ($route_name == 'jquery_form_validation.validation_settings') {
    $config = \Drupal::config("jquery_form_validation.settings");
    if (!empty($config)) {
      $json_apply = $config->get('demo_validation_enable');
      if ($json_apply) {
        $validataion_data = $config->get('demo_validation_data');
        if (!empty($validataion_data)) {
          if (!empty($validataion_data)) {
            $cleanedText = str_replace(["\r", "\n"], '', $validataion_data);
            $decodedJson = json_decode($cleanedText, TRUE);
            if (is_array($decodedJson)) {
              jquery_form_validation_remove_spaces($decodedJson);
              $formIds = explode("--", $form['#id']);
              $formId = $formIds[0];
              if (array_key_exists($formId, $decodedJson)) {
                $form['#attached']['library'][] = 'jquery_form_validation/form-validation';
                $form['#attached']['drupalSettings']['validationData']['formKey'] = $form['#id'];
                $form['#attached']['drupalSettings']['validationData']['rulesAndMessages'] = $decodedJson;
              }
            }
          }
        }
      }
    }
  }
  else {
    $config = \Drupal::config("jquery_form_validation.settings");
    if (!empty($config)) {
      $json_apply = $config->get('json_apply');
      if ($json_apply) {
        $names_fieldset = $config->get('names_fieldset');
        if (!empty($names_fieldset)) {
          unset($names_fieldset['actions']);
          foreach ($names_fieldset as $value) {
            $validataion_data = $value['validation_json'];
            if (!empty($validataion_data)) {
              $cleanedText = str_replace(["\r", "\n"], '', $validataion_data);
              $decodedJson = json_decode($cleanedText, TRUE);
              if (is_array($decodedJson)) {
                jquery_form_validation_remove_spaces($decodedJson);
                $formIds = explode("--", $form['#id']);
                $formId = $formIds[0];
                if (array_key_exists($formId, $decodedJson)) {
                  $form['#attached']['library'][] = 'jquery_form_validation/form-validation';
                  $form['#attached']['drupalSettings']['validationData']['formKey'] = $form['#id'];
                  $form['#attached']['drupalSettings']['validationData']['rulesAndMessages'] = $decodedJson;
                }
              }
            }
          }
        }
      }
    }
  }
}

/**
 * Remove spaces except from the "messages" part.
 */
function jquery_form_validation_remove_spaces($data) {
  foreach ($data as $key => &$value) {
    if (is_array($value)) {
      jquery_form_validation_remove_spaces($value);
    }
    else {
      if ($key !== 'messages') {
        $value = str_replace(' ', '', $value);
      }
    }
  }
}
